package esercizioFinale;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class ServiceImplVenditore implements ServiceVenditore {


    public void stamaAzioni() {
        System.out.println("=== Azioni Venditore ===\n" +
                "0 - aggiungi prodotto\n" +
                "1 - cancella prodotto\n" +
                "2 - lista prodotti in catalogo\n" +
                "3 - lista ordini\n" +
                "4 - invia ordine\n" +
                "5 - prodotto piú costoso nel catalogo\n" +
                "6 - prodotto piú costoso nell'ordine x\n" +
                "7 - applica sconto a ogni prodotto nell'ordine x del 10%\n" +
                "8 - saldi 50% su tutto il catalogo\n" +
                "9 - logout"
        );
    }

    @Override
    public void aggiungiProdotto(List<Prodotto> catalogoProdotti) {
        Date date2 = new Date(System.currentTimeMillis());
        Prodotto p = Prodotto.builder()
                .codice("006")
                .dataCreazione(date2)
                .nome("Giacomo")
                .prezzo(55.4)
                .peso(10.7)
                .dataModifica(date2)
                .build();
        catalogoProdotti.add(p);
        System.out.println("Ordine aggiunto " + catalogoProdotti);
    }

    @Override
    public void cancellaProdotto(List<Prodotto> catalogoProdotti) {
        Scanner scanner = new Scanner(System.in);
        String indice;
        if (!catalogoProdotti.isEmpty()){
            System.out.println("Quale prodotto vuoi cancellare?: ");
            indice = scanner.next();
            Prodotto p = catalogoProdotti.stream()
                    .filter(prodotto -> prodotto.getCodice().equals(indice))
                    .findFirst().orElse(null);
            if (p != null) {
                System.out.println("Verrà rimosso il prodotto" + p.getNome());
                catalogoProdotti.remove(p);
            }
        } {
            System.out.println("Il catalogo è vuoto");
        }

    }

    @Override
    public void listaProdotti(List<Prodotto> catalogoProdotti) {
        System.out.println(catalogoProdotti);

    }

    @Override
    public void listaOrdini(List<Prodotto> ordineCliente) {
        System.out.println("Ecco la tua lista degli ordini ");
        ordineCliente.forEach(System.out::println);
    }

    @Override
    public void prodottoMaxCostoInCatalogo(List<Prodotto> catalogoProdotti) {
        if (!catalogoProdotti.isEmpty()){
            System.out.println("il prodotto più costoso è " + catalogoProdotti.stream().max(Comparator.comparingDouble(Prodotto::getPrezzo)));
        } {
            System.out.println("Il catalogo è vuoto");
        }

    }

    @Override
    public void prodottoMaxCostoInOrdine() {
//TODO: da fare
    }

    @Override
    public void inviaOrdine() {
//TODO: da fare
    }

    @Override
    public void scontoOrdine10() {
//TODO: da fare
    }

    @Override
    public void saldi50Catalogo(List<Prodotto> catalogoProdotti){
        catalogoProdotti.stream().forEach(System.out::println);
        if (!catalogoProdotti.isEmpty()){
            for (int i=0; i< catalogoProdotti.size(); i++){
                System.out.println("Il catalogo è stato scontato del 50% ");
            }
        }
//TODO: da fare
    }


}
