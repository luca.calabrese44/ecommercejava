package esercizioFinale;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Ecommerce {
    public static List<Prodotto> catalogoProdotti = new ArrayList<>();
    public static List<Prodotto> carrelloProdotti = new ArrayList<>();
    public static List<Prodotto> ordineCliente = new ArrayList<>();
    public static List<Utente> utente = new ArrayList<>();

    public static ServiceVenditore serviceVenditore = new ServiceImplVenditore();
    public static ServiceCliente serviceCliente = new ServiceImplCliente();

    public static void main(String[] args) {
        Service service = new ServiceImpl();


        // set catalogo prodotti
        System.out.println("=== INIZIO ===");


        catalogoProdotti = service.riempiCatalogo();
        service.stampaCatalogo(catalogoProdotti);

        int scelta = -1;
        Scanner scannerProfili = new Scanner(System.in);
        while (scelta != 2) {
            service.stampaLogin();
            System.out.print("inserisci valore: ");
            scelta = scannerProfili.nextInt();
            switch (scelta) {
                case 0:
                    System.out.println("Benvenuto venditore");
                    azioniVenditore();
                    break;
                case 1:
                    System.out.println("Benvenuto cliente");
                    azioniCliente();
                    break;
                case 2:
                    System.out.println("Ciao ciao!!");
                    break;
                default:
                    System.out.println("Scelta non valida, inserire 0, 1 o 2");
                    break;
            }
        }
    }

    /**
     * azioni del venditore
     */
    public static void azioniVenditore() {
        int scelta = -1;
        Scanner scannerVenditore = new Scanner(System.in);
        while (scelta != 9) {
            serviceVenditore.stamaAzioni();
            System.out.print("inserisci valore: ");
            scelta = scannerVenditore.nextInt();
            switch (scelta) {
                case 0:
                    System.out.println("aggiungi prodotto");
                    serviceVenditore.aggiungiProdotto(catalogoProdotti);
                    break;
                case 1:
                    System.out.println("cancella prodotto");
                    serviceVenditore.cancellaProdotto(catalogoProdotti);
                    break;
                case 2:
                    System.out.println("lista prodotti in catalogo");
                    serviceVenditore.listaProdotti(catalogoProdotti);
                    break;
                case 3:
                    System.out.println("lista ordini");
                    serviceVenditore.listaOrdini(ordineCliente);
                    break;
                case 4:
                    System.out.println("invia ordine");
                    serviceVenditore.inviaOrdine();
                    break;
                case 5:
                    System.out.println("prodotto piú costoso nel catalogo");
                    serviceVenditore.prodottoMaxCostoInCatalogo(catalogoProdotti);
                    break;
                case 6:
                    System.out.println("prodotto piú costoso nell'ordine x");
                    serviceVenditore.prodottoMaxCostoInOrdine();
                    break;
                case 7:
                    System.out.println("applica sconto a ogni prodotto nell'ordine x del 10%");
                    serviceVenditore.scontoOrdine10();
                    break;
                case 8:
                    System.out.println("saldi 50% su tutto il catalogo");
                    serviceVenditore.saldi50Catalogo(catalogoProdotti);
                    break;
                case 9:
                    System.out.println("LogOut");
                    break;
                default:
                    System.out.println("Scelta non valida, inserire 0, 1, 2, 3, 4, 5, 6, 7, 8 o 9");
                    break;
            }
        }
    }

    /**
     * azioni del cliente
     */
    private static void azioniCliente() {
        int scelta = -1;
        Scanner scannerCliente = new Scanner(System.in);
        while (scelta != 10) {
            serviceCliente.stamaAzioni();
            System.out.print("inserisci valore: ");
            scelta = scannerCliente.nextInt();
            switch (scelta) {
                case 0:
                    System.out.println("visualizza prodotti");
                    serviceCliente.listaProdotti(catalogoProdotti);
                    break;
                case 1:
                    System.out.println("aggiungi prodotto al carrello");
                    serviceCliente.aggiungiProdottoCarrello(carrelloProdotti, catalogoProdotti);
                    break;
                case 2:
                    System.out.println("svuota carrello da tutti i prodotti");
                    serviceCliente.svuotaCarrello(carrelloProdotti);
                    break;
                case 3:
                    System.out.println("mostra prodotti nel carrello");
                    serviceCliente.stampaCarrello(carrelloProdotti);
                    break;
                case 4:
                    System.out.println("rimuovi prodotto dal carrello");
                    serviceCliente.rimuoviProdottoCarrello(carrelloProdotti);
                    break;
                case 5:
                    System.out.println("invia ordine dei prodotti presenti nel carrello");
                    serviceCliente.inviaOrdine(ordineCliente, carrelloProdotti);
                    break;
                case 6:
                    System.out.println("crea il tuo utente");
                    serviceCliente.sceltaNickname(utente);
                    break;
                case 7:
                    System.out.println("torna alla lista di scelta profilo");
                    serviceCliente.cambiaUtente(utente);
                    break;
                case 8:
                    System.out.println("totale costo carrello");
                    serviceCliente.totaleCostoCarrello(carrelloProdotti);
                    break;
                case 9:
                    System.out.println("totale peso carrello");
                    serviceCliente.totalePesoCarrello(carrelloProdotti);
                    break;
                case 10:
                    System.out.println("chiudi");
                    break;
                default:
                    System.out.println("Scelta non valida, inserire 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10");
                    break;
            }
        }
    }

}

