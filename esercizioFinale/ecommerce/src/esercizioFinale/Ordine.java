package esercizioFinale;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class Ordine {
    private String nome;
    private String numeroOrdine;
    private String nickname;
    private double prezzo;

    private double peso;

    private Date dataCreazione;

    private Date dataModifica;

    @Override
    public String toString() {
        return "{\"Prodotto\": {" + "\"codice\":\"" + numeroOrdine + "\"" + ", \"nome\":\"" + nome + "\"" + ", \"prezzo\":\"" + prezzo + "\"" + ", \"nickname\":\"" + nickname + "\"" + ", \"peso\":\"" + peso + "\"" + ", \"dataCreazione\":\"" + dataCreazione + "\"" + ", \"dataModifica\":\"" + dataModifica + "\"" + "}}" + "\n";
    }

}
