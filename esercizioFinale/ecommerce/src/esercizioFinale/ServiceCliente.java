package esercizioFinale;

import java.util.List;

public interface ServiceCliente {
    /**
     * metodo per stampare azioni cliente
     */
    void stamaAzioni();

    /**
     * elenco di prodotti nel catalogo
     */
    void listaProdotti(List<Prodotto> catalogoProdotti);

    /**
     * aggiungi prodotto al carrello
     */
    void aggiungiProdottoCarrello(List<Prodotto> carrelloProdotti, List<Prodotto> catalogoProdotti);

    /**
     * rimuovi prodotto nel carrello
     */
    void rimuoviProdottoCarrello(List<Prodotto> carrelloProdotti);

    /**
     * svuota carrello
     */
    void svuotaCarrello(List<Prodotto> carrelloProdotti);

    /**
     * invia ordine a venditore
     */
    void inviaOrdine(List<Prodotto> ordineCliente, List<Prodotto> carrelloProdotti);

    /**
     * totale costo prodotti nel carrello
     */
    void totaleCostoCarrello(List<Prodotto> carrelloProdotti);

    /**
     * totale peso prodotti nel carrello
     */
    void totalePesoCarrello(List<Prodotto> carrelloProdotti);
    void sceltaNickname(List<Utente> utente);
    void cambiaUtente(List<Utente> utente);
    void stampaCarrello(List<Prodotto> carrelloProdotti);

}
