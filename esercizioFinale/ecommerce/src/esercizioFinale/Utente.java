package esercizioFinale;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class Utente {
    private String nickname;
}
