package esercizioFinale;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class ServiceImplCliente implements ServiceCliente {

    @Override
    public void stamaAzioni() {
        System.out.println("=== Azioni cliente ===\n" +
                "0 - visualizza prodotti\n" +
                "1 - aggiungi prodotto al carrello\n" +
                "2 - svuota carrello da tutti i prodotti\n" +
                "3 - mostra carrello\n" +
                "4 - rimuovi prodotto dal carrello\n" +
                "5 - invia ordine dei prodotti presenti nel carrello\n" +
                "6 - torna alla lista di scelta profilo\n" +
                "7 - totale costo carrello\n" +
                "8 - totale peso carrello\n" +
                "9 - chiudi"
        );
    }

    @Override
    public void listaProdotti(List<Prodotto> catalogoProdotti) {
        System.out.println(catalogoProdotti);

    }

    @Override
    public void aggiungiProdottoCarrello(List<Prodotto> carrelloProdotti, List<Prodotto> catalogoProdotti) {
        // verificare che nel catalogoProdotti il prodotto esiste;
        // aggiungere il prodotto nel oggetto carrelloProdotti;
        // se non esiste errore;
        Date date2 = new Date(System.currentTimeMillis());
        Scanner scanner = new Scanner(System.in);
        if (catalogoProdotti.stream().map(Prodotto::getCodice).findFirst().isPresent()) {
            Prodotto p = Prodotto.builder()
                    .codice(scanner.next())
                    .dataCreazione(date2)
                    .nome("Giacomo")
                    .prezzo(55.4)
                    .peso(10.7)
                    .dataModifica(date2)
                    .build();
            carrelloProdotti.add(p);
            System.out.println("Ordine aggiunto " + carrelloProdotti);
        }
    }

    @Override
    public void rimuoviProdottoCarrello(List<Prodotto> carrelloProdotti) {
        Scanner scanner = new Scanner(System.in);
        String indice;
        if (!carrelloProdotti.isEmpty()){
            System.out.println("Quale prodotto vuoi cancellare dal carrello?: ");
            indice = scanner.next();
            Prodotto p = carrelloProdotti.stream()
                    .filter(prodotto -> prodotto.getCodice().equals(indice))
                    .findFirst().orElse(null);
            if (p != null) {
                System.out.println("Verrà rimosso il prodotto" + p.getNome());
                carrelloProdotti.remove(p);
            }
        } {
            System.out.println("Il carrello è vuoto");
        }
    }

    @Override
    public void svuotaCarrello(List<Prodotto> carrelloProdotti) {
        if (!carrelloProdotti.isEmpty()){
                System.out.println("Verranno eliminati tutti i prodotti nel carrello ");
                System.out.println("Sei sicuro?");
                System.out.println("Hai cancellato gli ordini dal carrello");
                carrelloProdotti.removeAll(carrelloProdotti);
        } {
            System.out.println("Il carrello è vuoto");
        }

    }

    @Override
    public void inviaOrdine(List<Prodotto> ordineCliente, List<Prodotto> carrelloProdotti) {
        ordineCliente.addAll(carrelloProdotti);
        carrelloProdotti.removeAll(carrelloProdotti);
        System.out.println("Hai inviato l'ordine al venditore " + ordineCliente);
    }

    @Override
    public void totaleCostoCarrello(List<Prodotto> carrelloProdotti) {
        if (!carrelloProdotti.isEmpty()){
            System.out.println("il costo è " + carrelloProdotti.stream().map(Prodotto::getPrezzo).reduce(0.0,
                    (a, b) -> a + b));
        } {
            System.out.println("Il catalogo è vuoto");
        }
    }

    @Override
    public void totalePesoCarrello(List<Prodotto> carrelloProdotti) {
        if (!carrelloProdotti.isEmpty()){
            System.out.println("il peso è " + carrelloProdotti.stream().map(Prodotto::getPeso).reduce(0.0,
                    (a, b) -> a + b));
        } {
            System.out.println("Il catalogo è vuoto");
        }

    }

    @Override
    public void sceltaNickname(List<Utente> utente){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Dammi un nickname ");
        Utente p = Utente.builder()
                .nickname(scanner.next())
                .build();
        utente.add(p);
        System.out.println("Hai creato l'utente " + p.getNickname());
    }
    @Override
    public void cambiaUtente(List<Utente> utente){

    }

    @Override
    public void stampaCarrello(List<Prodotto> carrelloProdotti){
        System.out.println(carrelloProdotti);

    }
}
